(function () {
  'use strict';

  module.exports = ['$resource', 'Config', function ($resource, Config) {
    return {
      weatherByCityName: $resource(Config.apiPath + 'q=:cityName&appid=' + Config.apiKey + '&units=metric', {
        cityName: '@cityName'
      }, {
        get: {
          method: 'GET'
        }
      }),
      weatherByCoords: $resource(Config.apiPath + 'lat=:cityLat&lon=:cityLon&appid=' + Config.apiKey + '&units=metric', {
        cityLat: '@cityLat',
        cityLon: '@cityLon'
      }, {
        get: {
          method: 'GET'
        }
      }),
      weatherIcons: $resource(Config.rootPath + 'shared/weather/icons.json', {}, {
        get: {
          method: 'GET'
        }
      })
    };
  }];

}());