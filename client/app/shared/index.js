module.exports = angular.module('shared', [
  require('./header').name,
  require('./navigation').name,
  require('./weather').name
])
  .constant('Config', require('./config'));