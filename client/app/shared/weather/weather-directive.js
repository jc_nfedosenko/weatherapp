(function () {
  'use strict';

  module.exports = ['Config', '$location', 'Query', function (Config, $location, Query) {


    function link(scope) {
      Query.weatherIcons.get().$promise.then(function (result) {
        scope.iconsObj = result;
      });
      scope.$on('cityChanged', function () {
        var code = scope.weather.id,
          iconName = scope.iconsObj[scope.weather.id].icon,
          prefix = 'wi wi-';
        if (!(code > 699 && code < 800) && !(code > 899 && code < 1000)) {
          iconName = 'day-' + iconName;
        }
        scope.icon = prefix + iconName;
      });
    }

    return {
      templateUrl: Config.rootPath + 'shared/weather/weather-view.html',
      link: link,
      replace: true
    };
  }];

}());