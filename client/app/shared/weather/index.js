module.exports = angular.module('shared.weather', [])
  .directive('weather', require('./weather-directive.js'));