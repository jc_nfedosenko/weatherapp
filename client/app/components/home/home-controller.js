(function () {
  'use strict';

  module.exports = ['$scope', 'Query', 'Geolocation', '$rootScope', '$timeout', function ($scope, Query, geolocation, $rootScope, $timeout) {
    $scope.cityName = '';
    $scope.notFound = false;
    $scope.weather = {};
    $scope.location = {};

    geolocation().then(function (position) {
      $scope.location.latitude = position.coords.latitude.toFixed(2);
      $scope.location.longitude = position.coords.longitude.toFixed(2);
      Query.weatherByCoords.get({
        cityLat: $scope.location.latitude,
        cityLon: $scope.location.longitude
      }).$promise.then(function (result) {
        if (result.cod === "404") {
          $scope.notFound = true;
        } else {
          $scope.weather.id = result.weather[0].id;
          $scope.weather.temperature = result.main.temp.toFixed(1);
          $scope.weather.description = result.weather[0].description;
          $scope.weather.main = result.main;
          $scope.weather.wind = result.wind;
          $scope.weather.coords = result.coord;
          $scope.weather.name = result.name;
          $scope.notFound = false;
          $timeout(function () {
            $rootScope.$broadcast('cityChanged');
          }, 500);
        }
      });
    }, function (reason) {
      console.log(reason);
    });

    function cityChangeCallback() {
      Query.weatherByCityName.get({
        cityName: $scope.cityName
      }).$promise.then(function (result) {
        if (result.cod === "404") {
          $scope.notFound = true;
        } else {
          $scope.weather.id = result.weather[0].id;
          $scope.weather.temperature = result.main.temp.toFixed(1);
          $scope.weather.description = result.weather[0].description;
          $scope.weather.main = result.main;
          $scope.weather.wind = result.wind;
          $scope.weather.coords = result.coord;
          $scope.weather.name = result.name;
          $scope.notFound = false;
          $rootScope.$broadcast('cityChanged');
        }
      });
    }

    $scope.reloadWeather = function () {
      cityChangeCallback();
      $scope.cityName = '';
    };

  }];

}());